///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
/////
///// @file megaton.cpp
///// @version 1.0
/////
///// @author Adrian Peng <apeng2@hawaii.edu>
///// @date 12_Feb_2022
/////////////////////////////////////////////////////////////////////////////////

#include "megaton.h"

double fromMegatonToJoule( double megaton ) {
      return megaton / MEGATONS_IN_A_JOULE ;
}

double fromJouleToMegaton( double joule ) {
      return joule * MEGATONS_IN_A_JOULE ;
}


